package bnfcompiler.compiler;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BnfCompiler {

    Compiler compiler = new Compiler();

    JTextPane textArea;
    JTextArea messageArea;

    public BnfCompiler() {

        JFrame frame = new JFrame("Компилятор");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));


        textArea = new JTextPane();
        Font font = new Font("sanserif", Font.PLAIN, 20);
        textArea.setFont(font);

        messageArea = new JTextArea(5, 10);
        messageArea.setLineWrap(true);
        font = new Font("sanserif", Font.PLAIN, 15);
        messageArea.setFont(font);
        messageArea.setEditable(false);
        messageArea.setText("Наберите код программы и нажмите кнопку 'Скомпилировать'");

        JScrollPane scrolledPane1 = new JScrollPane(textArea);
        scrolledPane1.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrolledPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        JScrollPane scrolledPane2 = new JScrollPane(messageArea);
        scrolledPane2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrolledPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrolledPane2.setMaximumSize(new Dimension(600, 200));

        JButton button = new JButton("Скомпилировать");
        font = new Font("sanserif", Font.BOLD, 15);
        button.setFont(font);
        button.addActionListener(new CompileListener());
        button.setAlignmentX(JComponent.CENTER_ALIGNMENT);

        panel.add(scrolledPane1);
        panel.add(Box.createVerticalStrut(5));
        panel.add(button);
        panel.add(Box.createVerticalStrut(5));
        panel.add(scrolledPane2);

        frame.pack();
        frame.add(panel);
        frame.setSize(600, 600);
        frame.setVisible(true);
    }

    public class CompileListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String program = textArea.getText();

            AbstractDocument doc;
            StyledDocument styledDoc = textArea.getStyledDocument();
            doc = (AbstractDocument) styledDoc;

            SimpleAttributeSet redAttribute = new SimpleAttributeSet();
            StyleConstants.setForeground(redAttribute, Color.red);

            SimpleAttributeSet normalAttribute = new SimpleAttributeSet();
            StyleConstants.setForeground(normalAttribute, Color.black);

            ArrayList<String> words = new ArrayList<String>();

            ArrayList<String> splitedText = compiler.split(program, ":", true);
            ArrayList<String> splitedTextTmp = new ArrayList<String>();
//        int j = 0;
            for (String textToSplit : splitedText) {
                ArrayList<String> textToSplitSplited = compiler.split(textToSplit, ",", true);
                splitedTextTmp.addAll(textToSplitSplited);
            }
            splitedText = splitedTextTmp;
            splitedTextTmp = new ArrayList<String>();
            for (String textToSplit : splitedText) {
                ArrayList<String> textToSplitSplited = compiler.split(textToSplit, ";", true);
                splitedTextTmp.addAll(textToSplitSplited);
            }
            splitedText = splitedTextTmp;
            splitedTextTmp = new ArrayList<String>();
            for (String textToSplit : splitedText) {
                ArrayList<String> textToSplitSplited = compiler.split(textToSplit, " ", false);
                splitedTextTmp.addAll(textToSplitSplited);
            }
            splitedText = splitedTextTmp;
            //words = splitedText;

            /**
             * Разбиение на слова кода программы
             */
            for (String word : splitedText) {

                if (!word.equals("")) {
                    if (word.contains("\n")) {
                        String lastWord = "";
                        String[] splited = word.split("[\\r* | \\n*]");
                        Pattern p = Pattern.compile("[\\r* | \\n*]+.+");
                        Matcher m = p.matcher(word);
                        p = Pattern.compile(".+[\\r* | \\n*]+");
                        Matcher m2 = p.matcher(word);
                        int i = 0;
                        boolean isInBegining = m.matches();
                        boolean isInEnd = m2.matches();
                        for (String subWord : splited) {
                            if (!subWord.equals("")) {
                                if (isInBegining) {
                                    words.add("\r\n" + subWord + " ");
                                } else if (isInEnd && i == splited.length - 1) {
                                    words.add(subWord + "\r\n");
                                } else {
                                    String wordToInsert = subWord + " ";
                                    if (i != splited.length - 1)
                                        wordToInsert = wordToInsert + "\r\n";
                                    words.add(wordToInsert);
                                }
                            }
                            i++;
                        }
                    } else {
                        word += " ";
                        words.add(word);
                    }
                }
            }

            if (!textArea.getText().equals("")) {
                if (compiler.compile(textArea.getText())) {

                    textArea.setText("");
                    int i = 0;
                    for (String word : words) {
                        try {
                            doc.insertString(doc.getLength(), word/* + " "*/, normalAttribute);
                        } catch (BadLocationException e1) {
                            e1.printStackTrace();
                        }
                        i++;
                    }

                    ArrayList<String> messages = compiler.getMessages();

                    messageArea.setText("");

                    // Выдаём сообщения
                    for (String message : messages) {
                        messageArea.append(message + "\n");
                    }

                    messageArea.append("Успешно скомпилировано");
                } else {
                    ArrayList<String> messages = compiler.getMessages();

                    messageArea.setText("");

                    // Выдаём сообщения
                    for (String message : messages) {
                        messageArea.append(message + "\n");
                    }

                    textArea.setText("");



                    // Цикл вывода слов зависит от типа ошибки: в мат. выражении или в просто в программе
                    if (compiler.isExpressionOk()) {
                        // Возможно, нужно: if (compiler.hasErrors()) ...
                        int i = 0;
                        for (String word : words) {
                            if (i == compiler.getIterator()) {
                                try {
                                    doc.insertString(doc.getLength(), word, redAttribute);
                                    // Чтобы сделать дальнейший ввод снова чёрным
                                    doc.insertString(doc.getLength(), "", normalAttribute);
                                } catch (BadLocationException e1) {
                                    e1.printStackTrace();
                                }
                            } else {
                                try {
                                    doc.insertString(doc.getLength(), word + "", normalAttribute);
                                } catch (BadLocationException e1) {
                                    e1.printStackTrace();
                                }
                            }

                            i++;
                        }
                    } else {
                        for (int i = 0; i < words.size(); i++) {
                            String word = words.get(i);
                            if (i == compiler.getBadExpressionBeginIndex()) {
                                // Попали в начальный индекс мат. выражения с ошибкой
                                // Теперь будем постепенно выводить слова мат. выражения, пока не поймём, что подошли к expressionSymbol (т.е., к ошибке)
                                try {
                                    String expression = "";
                                    boolean found = false;
                                    for (i = i; i <= compiler.getBadExpressionEndIndex(); i++) {
                                        word = words.get(i);
                                        String prevExpression = expression;
                                        expression += words.get(i);
                                        expression = expression.trim();
                                        if(found || expression.length() < compiler.getExpressionSymbol() + 1){
                                            // Тут мы либо уже нашли слово выражения с ошибкой, либо ещё не дошли до него
                                            doc.insertString(doc.getLength(), String.valueOf(words.get(i)), normalAttribute);
                                        } else {
                                            // Это означает, что в word содержится ошибка
                                            // Нужно подсчитать все символы и понять, какой подчеркнуть
                                            int errorIndex = 0;
                                            if(i == compiler.getBadExpressionBeginIndex()){
                                                errorIndex = compiler.getExpressionSymbol();
                                            } else {
                                                errorIndex = compiler.getExpressionSymbol() - prevExpression.length();
                                            }
                                            for (int k = 0; k < word.length(); k++) {
                                                char letter = word.charAt(k);
                                                if (k == errorIndex) {
                                                    // Выяснили, что тут ошибка
                                                    doc.insertString(doc.getLength(), String.valueOf(letter), redAttribute);
                                                } else {
                                                    doc.insertString(doc.getLength(), String.valueOf(letter), normalAttribute);
                                                }
                                            }
                                            found = true;
                                        }

                                    }
                                    // Нужно сделать корректное значение i
                                    i--;
                                } catch (BadLocationException e1) {
                                    e1.printStackTrace();
                                }
                            } else {
                                try {
                                    doc.insertString(doc.getLength(), word + "", normalAttribute);
                                } catch (BadLocationException e1) {
                                    e1.printStackTrace();
                                }
                            }

                        }
                    }
                }
            } else {
                messageArea.setText("Наберите код программы и нажмите кнопку 'Скомпилировать'");
            }
        }
    }


}